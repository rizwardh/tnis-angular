
import { Account } from './account';

export interface Transaction {
    id?: number;
    deposit: number;
    date?: number;
    account?: Account | number;
}