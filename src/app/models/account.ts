import { Transaction } from './transaction';

export class Account {
    id: number;
    name: string;
    email: string;
    createdDate: string;
    transactions: Transaction[];
}