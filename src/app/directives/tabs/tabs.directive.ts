import { Directive, ElementRef } from '@angular/core';

declare const $: any;

@Directive({
    selector: '[appTabs]'
})
export class TabsDirective {

    private get element() {
        return $(this.ref.nativeElement);
    }
  
    constructor(
          private ref: ElementRef
    ) { }
    
    ngOnInit(): void {
        this.element.on('click', 'a', (e: Event) => {
            e.preventDefault();
            $(e.target).tab('show');
        })
    }
    
    show(id: string) {
        this.element.find(`a[href="#${id}"]`).trigger('click');
    }

}
