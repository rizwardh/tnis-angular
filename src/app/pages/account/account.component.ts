import * as moment from 'moment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TabsDirective } from 'src/app/directives/tabs/tabs.directive';
import { Account } from 'src/app/models/account';
import { ApiResourceService } from 'src/app/services/api-resource/api-resource.service';
import { Transaction } from 'src/app/models/transaction';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

    models: Account[];
    @ViewChild(TabsDirective, { static: false }) tabs: TabsDirective;    

    isDetailActive: boolean = false;
    detailModel: Partial<Account> = {};
    depositModel = { amount: 0 };
    detailIndex: number;

    constructor(
        private apiResource: ApiResourceService
    ) { }

    async ngOnInit() {
        this.models = await this.apiResource.getAll('accounts', { limit: 1000 });
    }

    async beginEdit(model: Account, index: number) {
        this.detailIndex = index;
        this.detailModel = Object.assign({}, model);

        this.isDetailActive = true;
        this.tabs.show('add');

        if(!this.detailModel.transactions) {
            this.detailModel.transactions = await this.apiResource.send(`transactions/${model.id}`, 'GET');
        }
    }

    cancel() {
        if(this.isDetailActive) {
            this.isDetailActive = false;
            this.detailIndex = undefined;
        }

        this.detailModel = {};
        this.tabs.show('all');
    }

    async save() {
        const transaction: Transaction = {
            deposit: this.depositModel.amount,
            account: !this.isDetailActive? this.detailModel as Account : this.detailModel.id
        }
        const result = await this.apiResource.post('transactions', transaction)

        if(result) {
            transaction.account = undefined;
            transaction.date = moment.now();
            if(!this.isDetailActive) {
                this.detailModel.transactions = [ transaction ];
                this.models.push(this.detailModel as Account);
            }
            else {
                this.detailModel.transactions.push(transaction);
            }

            this.cancel();
        }
        else alert(this.apiResource.lastError);
    }

}
