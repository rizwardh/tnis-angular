import { Component, OnInit } from '@angular/core';
import { ApiResourceService } from 'src/app/services/api-resource/api-resource.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    count: {
        accounts?: number;
        transactions?: number;
    } = {};

    constructor(
        private apiResource: ApiResourceService
    ) { }

    ngOnInit() {
        this.fetchAccountsCount();
        this.fetchTransactionsCount();
    }

    async fetchAccountsCount() {
        const {count} = await this.apiResource.send('accounts/count', 'GET');
        return this.count.accounts = count;
    }

    async fetchTransactionsCount() {
        const {count} = await this.apiResource.send('transactions/count', 'GET');
        return this.count.transactions = count;
    }

}
