import { Pipe, PipeTransform } from '@angular/core';
import { Account } from 'src/app/models/account';

@Pipe({
    name: 'sum'
})
export class SumPipe implements PipeTransform {

    transform(value: Account): any {
        return value.transactions.reduce((x, y) => x + y.deposit, 0);
    }

}
