import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

type ResourceType = 'accounts' | 'transactions';
type QueryInfo = {
    limit?: number,
    page?: number
}

@Injectable({
    providedIn: 'root'
})
export class ApiResourceService {

    readonly baseUri = 'http://localhost:2112';

    private _lastError: string;

    get lastError() {
        return this._lastError;
    }

    constructor(
        private http: HttpClient
    ) { }

    async getAll<T = any>(type: ResourceType, query?: QueryInfo & { [key: string]: string | number }) {
        let params = new HttpParams();
        if(query)
            Object.keys(query).forEach(x => params = params.append(x, query[x].toString()));

        console.log(params);
        return await this.sendRequest<T>(`${this.baseUri}/${type}`, 'GET', params);
    }

    async post<T>(type: ResourceType, obj: T) {
        return await this.sendRequest<T>(`${this.baseUri}/${type}`, 'POST', null, obj);
    }

    async put<T>(type: ResourceType, id: string | number, obj: T) {
        return await this.sendRequest<T>(`${this.baseUri}/${type}/${id}`, 'PUT', null, obj);
    }

    async delete(type: ResourceType, id: string | number) {
        return await this.sendRequest<boolean>(`${this.baseUri}/${type}/${id}`, 'DELETE');
    }

    async send<T>(path: string, method?: 'GET'| 'POST' | 'PUT' | 'DELETE', params?: { [param: string]: string | string[] }, body?: any): Promise<T> {
        return await this.sendRequest<T>(`${this.baseUri}/${path}`, method, params, body);
    }

    private async sendRequest<T>(url: string, method?: 'GET'| 'POST' | 'PUT' | 'DELETE', params?: HttpParams | { [param: string]: string | string[] }, body?: any): Promise<T> {
        try {
            const res = await this.http.request<any>(method || 'GET', url, { params, body }).toPromise();
            if(res.status != 'success')
                return null;

            return res.result || true;
        }
        catch(ex) {
            const error = ex instanceof Error? ex.message : (typeof ex === 'string'? ex : JSON.stringify(ex));
            this._lastError = error;
            return null;
        }
    }
}
